patchage (1.0.4-1) UNRELEASED; urgency=medium

  * New upstream version 1.0.4
  * Use dh-compat instead of d/compat and bump to 13
  * d/control:
    + Bump S-V to 4.5.1
    + Update Vcs* fields
    + Use https for the homepage
    + Add me as uploader
    + Set RRR: no
    + Update B-Ds
  * d/copyright:
    + Fix d/copyright format
    + Use https for the urls
    + Update d/copyright year
    + d/copyright: Add me to the d/ section
  * d/rules:
    + Use a variable and python 3 for WAF
    + Add hardening
  * Remove obsolete d/unpack_waf.sh
  * Add d/upstream/metadata
  * Add d/upstream/signing-key.asc
  * Update d/watch file

 -- Dennis Braun <d_braun@kabelmail.de>  Sun, 10 Jan 2021 00:19:18 +0100

patchage (1.0.0~dfsg0-0.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop unnecessary Build-Depends on libgnomecanvasmm-2.6-dev and
    libglademm-2.4-dev (Closes: #885091)
  * Drop obsolete menu file and .xpm
  * Bump debhelper compat to 11
  * Bump Standards-Version to 4.1.2

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 23 Dec 2017 13:28:15 -0500

patchage (1.0.0~dfsg0-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * New upstream release (Closes: #747215):
    - Allow removing connections by selecting their handle and
      pressing delete.
    - Remove Raul dependency.
    - Switch from FlowCanvas to Ganv (much improved looks and performance).
    - Remove LASH support and simplify UI.
    - Support for DOT export for rendering with GraphViz.
    - Use XDG_CONFIG_HOME instead of ~/.patchagerc.
    - Make port colours configurable.
    - Support port pretty names via new Jack metadata API.
  * Drop build-dependency on libraul-dev.
  * Drop libflowcanvas-dev in favor of libganv-dev.
  * Drop debian/patches, the remaining delta has been merged upstream.
  * Update debian/watch.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Wed, 18 Jun 2014 13:37:16 +0100

patchage (0.5.0+dfsg0-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * Repack upstream tarball to get rid of the 'waf' blob (Closes: #654495):
  * debian/rules: Properly clean sources tree.

 -- Alessio Treglia <alessio@debian.org>  Wed, 11 Jan 2012 00:41:36 +0100

patchage (0.5.0-0.2) unstable; urgency=low

  * Non-maintainer upload.
  * Upload to unstable (Closes: #628672).

 -- Alessio Treglia <alessio@debian.org>  Sat, 11 Jun 2011 09:47:47 +0200

patchage (0.5.0-0.1) experimental; urgency=low

  * Non-maintainer upload.
  * New upstream release (Closes: #603138):
    - Changes in 0.5.0:
      + Auto-arrange interface modules sanely (align corresponding
        inputs/outputs)
      + Add -J (--no-jack) command line option
      + Add proper --help output and man page
      + Improve performance (dramatically for large setups)
      + Fancy console info/warning/error logging
      + Fix minor memory leaks and reduce memory consumption
      + Fix handling of ALSA duplex ports
      + Hide "split" module menu item when it is useless
      + Fix Jack D-Bus support
      + Mac OS X .app bundle port
      + Bump FlowCanvas dependency to 0.7.0
      + Add more/larger icons
      + Add missing COPYING file to distribution
      + Build system and code quality improvements
    - Changes in 0.4.5:
      + Install SVG icon
      + Fix compilation without Jack
      + Improve performance when dragging modules
      + Bump FlowCanvas dependency to 0.6.0
      + Upgrade to waf 1.5.18
  * debian/rules:
    - No need to apply hppa_parallel.patch anymore since hppa arch was
      dropped.
    - Rewrote to use pure DH7 short-form.
    - Pass --debug,--strict to ./waf configure.
  * debian/control:
    - Bump up build-dependencies on debhelper,libraul-dev,libflowcanvas-dev.
    - Drop lash support in favor of ladish.
    - Add jackd on Recommends.
    - Add ladish on Suggests.
    - Bump Standards.
  * debian/copyright:
    - Update copyright information.
    - Add license and copyright holder for waf.
    - Update as per DEP-5 rev.174.
  * debian/patches/0001-desktop_file.patch:
    - Remove Encoding field.
    - Remove Application from Category field.
    - Add trailing colon to the Category field value.
  * Remove unneeded debian/dirs file.
  * Add debian/install file rather than call dh_install to install every
    additional file.
  * Remove debian/patchage.{manpages,sgml}, upstream now provides a manpage.

 -- Alessio Treglia <alessio@debian.org>  Tue, 26 Apr 2011 12:14:00 +0200

patchage (0.4.4-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * debian/patches/hppa_parallel.patch:
    - Disable parallel build on hppa (Closes: #561305).
  * debian/rules:
    - Patch affected files on-the-fly.

 -- Luca Falavigna <dktrkranz@debian.org>  Sat, 15 May 2010 11:34:44 +0000

patchage (0.4.4-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Use convenience copy of waf, system wide waf package is being removed
    from Debian (Closes: #571711).
  * Build-depend on python, convenience copy of waf needs it.

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 11 Apr 2010 16:41:01 +0200

patchage (0.4.4-1) unstable; urgency=low

  * New upstream release (closes: #560524)
  * debian/source/format: 3.0 quilt for .bz2 upstream
  * debian/watch: switch to bz2
  * debian/rules: use debhelper ninja foo, switch to waf
  * debian/control: add build-depends on waf, remove autotools-dev
  * debian/patchage.manpages: add list of manpages
  * debian/rules: add patchage.svg missing from waf install

 -- Paul Brossier <piem@debian.org>  Sat, 12 Dec 2009 16:28:59 +0100

patchage (0.4.2-1) unstable; urgency=low

  * New upstream release (closes: #468933)
  * debian/control:
    - add build-depends on liblash-dev (closes: #452236)
    - add build-depends on libboost-dev, libflowcanvas-dev and libraul-dev
    - add Homepage and Vcs-* fields, remove Homepage from description
  * debian/watch, debian/copyright: updated to new download location
  * debian/compat: bump to 7
  * debian/rules: do not ignore clean error
  * debian/menu: change from Apps to Applications
  * debian/docs: NEWS removed upstream debian/docs
  * debian/patchage.xpm: updated text icon from upstream version
  * debian/patchage.desktop: remove, use upstream version

 -- Paul Brossier <piem@debian.org>  Fri, 20 Nov 2009 09:28:38 +0100

patchage (0.2.3-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Build-Depend on libgtkmm-2.4-dev instead of libgtkmm2.0-dev.
    Closes: #431214

 -- Andreas Barth <aba@not.so.argh.org>  Fri, 20 Jul 2007 21:19:32 +0000

patchage (0.2.3-2) unstable; urgency=low

  * Rebuild against new libgkt+_+asdok
  * Update url in debian/{watch,copyright}

 -- Paul Brossier <piem@debian.org>  Sun, 11 Dec 2005 16:04:11 +0000

patchage (0.2.3-1) unstable; urgency=low

  * New upstream release

 -- Paul Brossier <piem@debian.org>  Fri, 30 Sep 2005 02:32:10 +0100

patchage (0.2.0-1) unstable; urgency=low

  * Initial release (closes: #318618)

 -- Paul Brossier <piem@debian.org>  Sun, 24 Jul 2005 14:46:39 +0100
